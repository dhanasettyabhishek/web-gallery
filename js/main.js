// import Vue from 'vue'
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Vue.use(BootstrapVue)

var app = new Vue({
    el: "#app",
    data:{
        data_: [],
        // 'fullWidthImage': false,
        count: 0,
        // index = 0
    },
    mounted() {
        axios.get("https://picsum.photos/v2/list").then(
            response => {this.data_ = response.data}
        )
    },
    methods: {
        updateCnt(index) {
          this.count = index;
        }
      }
})

var app2 = new Vue({
    el: "#app1",
    data:{
        title: "Web Gallery",
        description: "From our beloved artists!",
    }
})